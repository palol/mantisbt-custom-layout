<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once './includes/RoutingHelper.php';
/** CustomLayout plugin declaration according to 
 * https://mantisbt.org/forums/viewtopic.php?t=25373
 */
 //include_once './includes/RoutingHelper.php';
 //use \RoutingHelper;

class CustomLayoutPlugin extends MantisPlugin {
    
    private string $customLayoutAddress;
    
    private string $customFooter;
        
    /**
     * Properties will be used by Mantis plugin's system
     */
    public function register() 
    {
      $this->customLayoutAddress = '/plugins/CustomLayout';//RoutingHelper::getRootPath().'plugins/CustomLayout';// //RoutingHelper::getPluginAddress();
      $this->customFooter = '<div class="col-12 clearfix"></div>'
              . '<div class="container row clearfix">'
              . '<div class="col-md-6 col-xs-12 no-padding">%s</div>'
              . '<div class="col-md-6 col-xs-12"><img src="' . $this->customLayoutAddress . '/files/custom_logo.png" alt="test" id="custom-logo" /></div>'
              . '</div>';
      
      
      $this->name = plugin_lang_get('plugintitle');
      $this->description = plugin_lang_get('plugindescription');
      $this->page = 'settings';

      $this->version = '0.1.2';
      $this->requires = array(
        "MantisCore" => "2.0.0",
      );

      $this->author = 'Luc';
      $this->contact = 'donotbother@me.org';
      $this->url = plugin_lang_get('repo');
    }
    
    /**
     * Binds the internal method to the given event hook.
     * @see ~/core/events_inc.php
     */
    public function hooks() 
    {
	return [
	    //'EVENT_PLUGIN_INIT' => 'initPlugin',
            'EVENT_LAYOUT_RESOURCES' => 'addCustomCSS',
	    //'EVENT_LAYOUT_BODY_BEGIN' => 'customiseBody',
	    //'EVENT_LAYOUT_PAGE_HEADER' => 'customiseHeader',
	    //'EVENT_MENU_MAIN_FRONT' => 'customiseMainMenu',
	    'EVENT_LAYOUT_BODY_END' => 'customiseFooter'
	];
    }
    
    /**
     * initPlugin TODO
     * @return null
     */
    public function initPlugin() 
    {
	return null;
    }
    
    /**
     * addCustomCSS
     * @return void
     */
    public function addCustomCSS() 
    {
        echo '<link href="' . plugin_file( 'styles/custom.css' ) . '" rel="stylesheet" type="text/css" />';
    }
    
    public function customiseBody() 
    {
	return true;
    }
    
    public function customiseHeader() 
    {
	return true;
    }
    
    public function customiseMainMenu() 
    {
	return true;
    }
    
    public function customiseFooter() 
    {
        $message = 'dfdffd';
        /*include_once './includes/RoutingHelper.php';
        $message .= RoutingHelper::getRootPath();*/
        $message .= '<br>dhdfhdfhdfhdfhdf';
        /*$message .= RoutingHelper::getPluginPath();*/
        //$footer = '<div class="col-12 clearfix"></div><div class="row clearfix"><div class="col-md-6 col-xs-12 no-padding">%s</div><div class="col-md-6 col-xs-12"><img src="' . $this->customLayoutAddress . '/files/img/custom_logo.png" alt="test" id="custom-logo" /></div></div>';
	echo sprintf( $this->customFooter, $message );
    }
}
