<?php


/**
 * Description of RoutingHelper
 *
 * @author lu
 */
class RoutingHelper {
    
    static function getRootPath() 
    {
        return dirname(dirname(dirname(dirname(dirname(__FILE__)))));
    }
    
    static function getPluginPath()
    {
        return dirname(dirname(dirname(__FILE__)));
    }
    
    static function getPluginAddress() 
    {
        return substr(self::getPluginPath(), strlen(self::getRootPath()));
    }
    
    static function andCheckIfSubfolder() 
    {
       return dirname($_SERVER['SCRIPT_NAME']);
    }
    
    static function dengerouslyGetRootURL() 
    {
        return (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
    }
}
