<?php
layout_page_header();
layout_page_begin();
?>

<div class="container">
    <hgroup class="">
	<h2><?php echo plugin_lang_get( 'plugintitle' ) . ' ' . plugin_lang_get( 'settings' ); ?></h2>
	<h4><?php echo plugin_lang_get( 'plugindescription' ); ?></h4>
    </hgroup>

    <main class="main-content">
	<p>test</p>
    </main>

    <div class="h-sidebar">
	<div class="btn-group center">
	    <div><a href="/" target="_self" class="btn btn-secondary"><?php echo plugin_lang_get( 'home' ); ?></a></div>
	</div>
    </div>
</div>

<?php
layout_page_end();