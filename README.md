# CUSTOM LAYOUT FOR MANTIS-BT

## Features
This plugin aims at customising the look and feel of the back-end of MantisBT.
<!-- * This plugin changes the logo of the login screen with the image found in this plugin folder `file/img/custom-logo.png`.
[//]: # * It changes the favicon with the image found in this plugin folder `file/img/favicon.ico`. -->

## Installation
* `git clone https://gitlab.com/palol/mantisbt-custom-layout.git` to the `/plugins` folder of your installation of MantisBT.
* Run `./plugins/mantisbt-custom-layout-master/files/install/install.sh` or `mv mantisbt-custom-layout-master/ CustomLayout/` from the `/plugins` folder.

Alternatively use https://gitlab.com/palol/mantisbt-external-plugins and add this plugin as an external plugin according to the instructions.

## Changelog
v0.1.0: Work in progress